import { useState } from 'react'
import { ethers } from 'ethers'
import Greeter from './artifacts/contracts/Greeter.sol/Greeter.json'
import './App.css'

const greeterAddress = '0x5FbDB2315678afecb367f032d93F642f64180aa3'

function App() {
  const [message, setMessage] = useState("");
  const [greetMsg, setGreetMsg] = useState("")

  // helper func

  async function requestAccount(){
    await window.ethereum.request({method: "eth_requestAccounts"}) 
  }

  async function fetchGreeting(){
    if(typeof window.ethereum != undefined){
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const contract = new ethers.Contract(greeterAddress, Greeter.abi, provider)

      try {
        const data = await contract.greet();
        setGreetMsg(data)
        console.log(data);
      }catch(err){
        console.log("Error: ", err);
      }
    }
  }

  async function setGreeting() {
    if(!message) return;

    if(typeof window.ethereum != undefined){
      await requestAccount();

      const provider = new ethers.providers.Web3Provider(window.ethereum)

      const signer = provider.getSigner();

      const contract = new ethers.Contract(greeterAddress, Greeter.abi, signer);
      const transaction = await contract.setGreeting(message);

      setMessage("");
      await transaction.wait()
      fetchGreeting()
    }

  }

  return (
    <div className="container">
      <h1>Greeter.sol</h1>
      <h2>Full Stack Dapp</h2>
      <h3>{greetMsg}</h3>
      <button onClick={fetchGreeting}>Fetch Greeting</button>
      <button onClick={setGreeting}>Set Greeting</button>
      <input type="text" placeholder='Set Greeting Message' onChange={e => setMessage(e.target.value)} value={message}/>      
    </div>
  )
}

export default App
